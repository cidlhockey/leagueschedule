#!/usr/bin/python3
# Licensed under Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
# https://creativecommons.org/licenses/by-sa/3.0/
#
# Based on: https://stackoverflow.com/questions/1037057/how-to-automatically-generate-a-sports-league-schedule
#
# Changes were made to the code to allow it to compile and customize the divisions and
# team names for the CIDL's 2020-21 season.

# From the list that's generated, take enough match-ups to fill one day's worth of games. For noviceDivison, you'll
# grab 2 lines for each Sunday, and the entire set will repeat every 3 weeks for 4 teams. (one less than the total
# number of teams) For intermediateDivision, grab 3 lines for each Sunday, and it will repeat every 5 weeks.

noviceDivision = ["Grizzly Goons", "Crocodiles", "Knights", "Vicious Squirrels"]
intermediateDivision = ["Wolverines", "Benders", "Hooligans", "DSM Whalers", "Wild Goose", "Watermelon Warriors"]


def create_schedule(team_list):
    """ Create a schedule for the teams in the list and return it"""
    s = []

    if len(team_list) % 2 == 1:
        team_list = team_list + ["BYE"]

    for i in range(len(team_list) - 1):

        mid = int(len(team_list) / 2)
        l1 = team_list[:mid]
        l2 = team_list[mid:]
        l2.reverse()

        # Switch sides after each round
        if i % 2 == 1:
            s = s + [zip(l1, l2)]
        else:
            s = s + [zip(l2, l1)]

        team_list.insert(1, team_list.pop())

    return s


def main():
    for noviceRound in create_schedule(noviceDivision):
        for noviceMatch in noviceRound:
            print(noviceMatch[0] + ",vs.," + noviceMatch[1])
    print()

    for intermediateRound in create_schedule(intermediateDivision):
        for intermediateMatch in intermediateRound:
            print(intermediateMatch[0] + ",vs.," + intermediateMatch[1])
    print()


if __name__ == "__main__":
    main()
